/// \file

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "audiomanip.h"

/// \fn MainWindow::MainWindow(QWidget *parent)
/// \~English
/// \brief GUI Constructor
/// \~Spanish
/// \brief Constructor del GUI
MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    MediaPlayer = new QMediaPlayer(this);

}

/// \fn MainWindow::~MainWindow()
/// \~English
/// \brief GUI Destructor
/// \~Spanish
/// \brief Destructor del GUI
MainWindow::~MainWindow()
{
    if (MediaPlayer != NULL) delete MediaPlayer;
    if (ui) delete ui;
}

/// \fn void MainWindow::on_actionExit_triggered()
/// \~English
/// \brief Exits the application
/// \~Spanish
/// \brief Sale del programado
void MainWindow::on_actionExit_triggered()
{
  ws.~WaveSound() ;
  QApplication::quit();
}

/// \fn void MainWindow::on_actionAbout_triggered()
/// \~English
/// \brief Display about message
/// \~Spanish
/// \brief Despliega mensaje sobre la aplicacion.
void MainWindow::on_actionAbout_triggered()
{
    //display a message box
    QMessageBox::about(this, tr("About Wave Editor"),
    tr("The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY "
       "OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE."));
}

/// \fn void MainWindow::on_Filter_Box_currentIndexChanged(int index)
/// \~English
/// \brief When filter box is set to a pan or fade filter the pan/fade
/// filter length box in enabled, otherwise it is disabled.
/// \param index Index position of the filter option.
/// \~Spanish
/// \brief Cuando la caja de filtro es cambiada a un filtro de panoramica,
/// desbanecimiento de sonido o incremento de sonido, la caja del largo de la
/// panoramica o del desbanecimiento se pone activa, de los contranio se
/// desactiva.
/// \param index Posicion del indice de la opcion del filtro.
void MainWindow::on_Filter_Box_currentIndexChanged(int index)
{
        ui->Fade_Pan_Lenght->setEnabled(bool(index));
        ui->label_3->setEnabled(bool(index));
}

/// \fn void MainWindow::setFilesAndButtonStatus(const QString &fileName)
/// \~English
/// \brief Put the values of the path of the audio file searched and selected
/// and enables and disbles the buttons.
/// \param fileName the path of the audio file.
/// \~Spanish
/// \brief Pone los valores del camino a el archivo de audio buscando y
/// seleccionado, y activa y desactiva los botones.
/// \param fileName el camino del archivo de audio
void MainWindow::setFilesAndButtonStatus(const QString &fileName) {
    AudioInFile = AudioOutFile = fileName;
    AudioOutFile.truncate(AudioInFile.size()-3);
    AudioOutFile = AudioOutFile+"out.wav";

    ui->Audio_In->addItem(AudioInFile);
    ui->Audio_In->setCurrentText(AudioInFile);
    ui->Audio_Out->addItem(AudioOutFile);
    ui->Audio_Out->setCurrentText(AudioOutFile);

    ui->Play_Audio_In->setEnabled(true);
    ui->Stop_Audio_In->setEnabled(true);

    ui->Play_Audio_Out->setEnabled(false);
    ui->Stop_Audio_Out->setEnabled(false);

    ui->run_filter->setEnabled(true);
}

/// \fn void MainWindow::on_Action_Input_Audio_File_triggered()
/// \~English
/// \brief Display the window to search and select an input audio file.
/// \~Spanish
/// \brief Despliega la pantalla para buscar y seleccionar un archivo de audio de entrada.
void MainWindow::on_Action_Input_Audio_File_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.wav)"));
    Frames = ws.getAudioSamples(fileName);
    setFilesAndButtonStatus(fileName);
}


/// \fn void MainWindow::on_Search_Audio_In_clicked()
/// \~English
/// \brief Set the Input Audio File with the Search Button
/// \~Spanish
/// \brief Establece el archivo de entrada de audio con el Boton de Busqueda
/// (Search Button)
void MainWindow::on_Search_Audio_In_clicked()
{
    on_Action_Input_Audio_File_triggered();
}

//Set Input Audio File With Combo Box
/// \fn void MainWindow::on_Audio_In_currentIndexChanged(const QString &arg1)
/// \~English
/// \brief Set the Input Audio File with the Text Box value
/// \param arg1 path to audio file
/// \~Spanish
/// \brief Establece el archivo de entrada de audio con el valor en la
/// caja de texto.
/// \param arg1 camino al archivo de audio
void MainWindow::on_Audio_In_currentIndexChanged(const QString &arg1)
{
    setFilesAndButtonStatus(arg1);
}


/// \fn void MainWindow::on_Action_Output_Audio_File_triggered()
/// \~English
/// \brief Display the window to search and select an input audio file.
/// \~Spanish
/// \brief Despliega la pantalla para buscar y seleccionar un archivo de audio de entrada.
void MainWindow::on_Action_Output_Audio_File_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Files (*.wav)"));
    AudioOutFile = fileName;
    ui->Audio_Out->addItem(AudioOutFile);
    ui->Audio_Out->setCurrentText(AudioOutFile);
}

/// \fn void MainWindow::on_Search_Audio_Out_clicked()
/// \~English
/// \brief Set the Output Audio File with the Search Button
/// \~Spanish
/// \brief Establece el archivo de salida de audio con el Boton de Busqueda
/// (Search Button)
void MainWindow::on_Search_Audio_Out_clicked()
{
    on_Action_Output_Audio_File_triggered();
}

/// \fn void MainWindow::on_Audio_Out_currentIndexChanged(const QString &arg1)
/// \~English
/// \brief Set Output Audio File With the value in the Text Box
/// \param arg1 Path to the audio file.
/// \~Spanish
/// \brief Establece el archivo de salida de audio con el valor en la
/// caja de texto
/// \param Camino al archivo de audio.
///
void MainWindow::on_Audio_Out_currentIndexChanged(const QString &arg1)
{
    AudioOutFile = arg1;
}



/// \fn void MainWindow::on_Play_Audio_In_clicked()
/// \~English
/// \brief Play the Input Audio
/// \~Spanish
/// \brief Toca el audio de entrada
void MainWindow::on_Play_Audio_In_clicked()
{
    MediaPlayer->stop();
    MediaPlayer->setMedia(QUrl::fromLocalFile(AudioInFile));
    MediaPlayer->stop();
    MediaPlayer->play();
}

/// \fn void MainWindow::on_Stop_Audio_In_clicked()
/// \~English
/// \brief Stop playing the Input Audio
/// \~Spanish
/// \brief Para de tocar el audio de entrada
void MainWindow::on_Stop_Audio_In_clicked()
{
    MediaPlayer->stop();
}

/// \fn void MainWindow::on_Play_Audio_Out_clicked()
/// \~English
/// \brief Play the Output Audio
/// \~Spanish
/// \brief Toca el audio de salida
void MainWindow::on_Play_Audio_Out_clicked()
{
    MediaPlayer->stop();
    MediaPlayer->setMedia(QUrl::fromLocalFile(AudioOutFile));
    MediaPlayer->stop();
    MediaPlayer->play();

}

/// \fn void MainWindow::on_Stop_Audio_Out_clicked()
/// \~English
/// \brief Stop playing the Output Audio
/// \~Spanish
/// \brief Para de tocar el audio de salida
void MainWindow::on_Stop_Audio_Out_clicked()
{
    MediaPlayer->stop();
}

/// \fn void MainWindow::on_Play_Audio_In_clicked()
/// \~English
/// \brief Run the selected audio filter
/// \~Spanish
/// \brief Corre el filtro de audio seleccionado.
void MainWindow::on_run_filter_clicked()
{
    Frames = ws.getAudioSamples(AudioInFile);

    switch(ui->Filter_Box->currentIndex())
    {
        case 0:
            RemoveVocals(Frames, ws.frameCount()) ;
            break;

        case 1:
            AudioFadeIn(Frames, ws.frameCount(), ui->Fade_Pan_Lenght->value());
            break;

        case 2:
            AudioFadeOut(Frames, ws.frameCount(), ui->Fade_Pan_Lenght->value());
            break;

        case 3:
            LeftToRight(Frames, ws.frameCount(), ui->Fade_Pan_Lenght->value());
            break;
    };

    cout<<endl<<"running filter"<<endl;
    cout<<endl<<AudioOutFile.toStdString()<<endl;

    ws.writeNewAudioFile(AudioOutFile);
    ui->Play_Audio_Out->setEnabled(true);
    ui->Stop_Audio_Out->setEnabled(true);
}









