var searchData=
[
  ['audiobuffer',['AudioBuffer',['../audiomanip_8h.html#a03d94cc61b0c5794b5aeb0e8390e7e0d',1,'audiomanip.h']]],
  ['audiofadein',['AudioFadeIn',['../audiomanip_8cpp.html#a88857e657ef3840063e4ea5e9e898dba',1,'AudioFadeIn(AudioBuffer frames[], int N, int fade_length):&#160;audiomanip.cpp'],['../audiomanip_8h.html#a88857e657ef3840063e4ea5e9e898dba',1,'AudioFadeIn(AudioBuffer frames[], int N, int fade_length):&#160;audiomanip.cpp']]],
  ['audiofadeout',['AudioFadeOut',['../audiomanip_8cpp.html#a980252d21947f1aa1bd45d1ba3938121',1,'AudioFadeOut(AudioBuffer frames[], int N, int fade_length):&#160;audiomanip.cpp'],['../audiomanip_8h.html#a980252d21947f1aa1bd45d1ba3938121',1,'AudioFadeOut(AudioBuffer frames[], int N, int fade_length):&#160;audiomanip.cpp']]],
  ['audiomanip_2ecpp',['audiomanip.cpp',['../audiomanip_8cpp.html',1,'']]],
  ['audiomanip_2eh',['audiomanip.h',['../audiomanip_8h.html',1,'']]]
];
